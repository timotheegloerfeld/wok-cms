
import Login from './components/Login.vue'
import Dashboard from './components/Dashboard.vue'
import CategoryList from './components/CategoryList/CategoryList.vue'
import MealList from './components/CategoryList/MealList/MealList.vue'
import PictureList from './components/PictureList/PictureList.vue'
import UserList from './components/UserList/UserList.vue'
import UserComposer from './components/UserList/UserComposer.vue'


const routes = [
	{ 
		path:'/login', 
		component: Login,
	},
	{ 
		path:'/', 
		redirect: 'categories', 
		component: Dashboard,
		children: [
			{
				path: 'categories',
				component: CategoryList,
				children: [
					{
						path: ':name',
						component: MealList,
					},	
				]
			},
			{
				path: 'pictures',
				component: PictureList,
			},
			{
				path: 'users',
				component: UserList,
				children: [
					{
						path: ':name',
						component: UserComposer,
						props: true,
					},
				]
			}
		],
	}
]

export default routes
