import api from '../api.js'

// CRUD actions for Models: Category, Meal, Image, User

const actions = {

	login (context, user)
	{
		return new Promise((resolve, reject) => {	
			api.post('/login', user)
				.then(response => {
					context.commit('login', response.data);
					resolve();
				})
				.catch(error => {
					context.commit('setError', error);
					reject();
				});
			})
	},
	logout (context, user)
	{
		return new Promise((resolve, reject) => {
			context.commit('logout');
			resolve();
		});
	},

	initData (context)
	{
		return new Promise((resolve, reject) => {
			if(localStorage.wok_user !== undefined){
				let user = JSON.parse(localStorage.wok_user);
				Object.assign(api.defaults, {headers: {Authorization: 'Bearer ' + user.api_token}});
			} else {
				reject();
			}	
			context.dispatch('retrieveCategories');
			context.dispatch('retrievePictures');
			context.dispatch('retrieveUsers');
			resolve()
		});
	},

	/*
	 * CRUD for Model Category
	 *
	 * Since Meals depends on Categories, dispatch retrieve Meals
	 * after the categories mutation
	 */

	retrieveCategories (context)
	{
		api.get('/categories')
			.then(response => {
				context.commit('retrieveCategories', response.data);
				context.dispatch('retrieveMeals');
			})
			.catch(error =>	context.commit('setError', error));
	},
	createCategory (context, category)
	{
		api.post('/categories', category)
			.then(response => context.commit('createCategory', response.data))
			.catch(error => context.commit('setError', error));
	},
	updateCategory (context, category)
	{
		api.put('/categories/'+category.id, category)
			.then(response => context.commit('updateCategory', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	deleteCategory (context, category)
	{
		api.delete('/categories/'+category.id)
			.then(context.commit('deleteCategory', category))
			.catch(error => context.commit('setError', error));
	},

	// CRUD for Model Meal
	
	retrieveMeals (context)
	{
		api.get('/meals')
			.then(response => context.commit('retrieveMeals', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	createMeal (context, meal)
	{
		api.post('/meals', meal)
			.then(response => context.commit('createMeal', response.data))
			.catch(error => context.commit('setError', error));
	},
	updateMeal (context, meal)
	{
		api.put('/meals/'+meal.id, meal)
			.then(response => context.commit('updateMeal', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	deleteMeal (context, meal)
	{
		api.delete('/meals/'+meal.id)
			.then(context.commit('deleteMeal', meal))
			.catch(error => context.commit('setError', error));
	},
	
	// CRUD for Model Image
	
	retrievePictures (context)
	{
		api.get('/pictures')
			.then(response => context.commit('retrievePictures', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	createPicture (context, picture)
	{
		api.post('/pictures', picture)
			.then(response => context.commit('createPicture', response.data))
			.catch(error => context.commit('setError', error));
	},
	updatePicture (context, picture)
	{
		api.put('/pictures/'+picture.id, picture)
			.then(response => context.commit('updatePicture', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	deletePicture (context, picture)
	{
		api.delete('/pictures/'+picture.id)
			.then(context.commit('deletePicture', picture))
			.catch(error => context.commit('setError', error));
	},
	
	// CRUD for Model User
	
	retrieveUsers (context)
	{
		api.get('/users')
			.then(response => context.commit('retrieveUsers', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	createUser (context, user)
	{
		console.log(user);
		api.post('/users', user)
			.then(response => context.commit('createUser', response.data))
			.catch(error => context.commit('setError', error));
	},
	updateUser (context, user)
	{
		api.put('/users/'+user.id, user)
			.then(response => context.commit('updateUser', response.data))
			.catch(error =>	context.commit('setError', error));
	},
	deleteUser (context, user)
	{
		api.delete('/users/'+user.id)
			.then(context.commit('deleteUser', user))
			.catch(error => context.commit('setError', error));
	},
};

export default actions
