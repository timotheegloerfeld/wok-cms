import mutations from './mutations.js'
import actions from './actions.js'

const store = {
	state:
	{
		categories: [],
		activeCategory: null,
		pictures: [],
		users: [],
		activeUser: null,
		error: {message: ""},
	},
	getters:
	{
		categories: state => state.categories,
		activeCategory: state => state.activeCategory,
		pictures: state => state.pictures,
		users: state => state.users,
		activeUser: state => state.activeUser,
		error: state => state.error,
	},
	mutations: mutations,
	actions: actions,
};

export default store
