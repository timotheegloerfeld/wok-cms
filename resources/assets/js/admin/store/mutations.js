
const mutations = {

	login(state, user)
	{
		localStorage.setItem('wok_user', JSON.stringify(user));
	},
	logout(state)
	{
		localStorage.removeItem('wok_user');
	},

	setError(state, error)
	{
		console.log(error);
	},

	// CRUD fo Model Category

	retrieveCategories (state, categories)
	{
		categories.forEach(category => category.meals = [])
		state.categories = categories;
	},
	createCategory (state, category)
	{
		category.meals = [];
		state.categories.push(category);
	},
	updateCategory (state, category)
	{
		var oldCategory = state.categories.find(categoriesItem => categoriesItem.id === category.id);
		oldCategory = category;
	},
	deleteCategory (state, category)
	{
		var index = state.categories.findIndex(categoriesItem => categoriesItem.id === category.id);
		state.categories.splice(index, 1);
	},

	setActiveCategory (state, category)
	{
		state.activeCategory = category;
	},

	// cRUD for Model Meal with implementation of its relationship to Model Category

	retrieveMeals (state, meals)
	{
		meals.forEach(meal => {
			var parentCategory = state.categories.find(
				categoriesItem => categoriesItem.id === meal.category_id
			);
			parentCategory.meals.push(meal);
		});

	},
	createMeal (state, meal)
	{
		var parentCategory = state.categories.find(
			categoriesItem => categoriesItem.id === meal.category_id
		);
		parentCategory.meals.push(meal);
	},
	updateMeal (state, meal)
	{
		var parentCategory = state.categories.find(
			categoriesItem => categoriesItem.id === meal.category_id
		);
		var oldMeal = parentCategory.meals.find(mealsItem => mealsItem.id === meal.id);
		oldMeal = meal;
	},
	deleteMeal (state, meal)
	{
		var parentCategory = state.categories.find(
			categoriesItem => categoriesItem.id === meal.category_id
		);
		console.log(meal);
		var index = parentCategory.meals.findIndex(mealsItem => mealsItem.id === meal.id);
		parentCategory.meals.splice(index, 1);
	},
	
	// CRUD for Model Picture
	
	retrievePictures (state, pictures)
	{
		state.pictures = pictures;
	},
	createPicture (state, picture)
	{
		state.pictures.push(picture);
	},
	updatePicture (state, picture)
	{
		let oldPicture = state.pictures.find(picturesItem => picturesItem.id === picture.id);
		oldPicture = picture;
	},
	deletePicture (state, picture)
	{
		let index = state.pictures.findIndex(picturesItem => picturesItem.id === picture.id);
		state.pictures.splice(index, 1);
	},

	// CRUD for Model User
	
	retrieveUsers (state, users)
	{
		state.users = users;
	},
	createUser (state, user)
	{
		state.users.push(user);
	},
	updateUser (state, user)
	{
		var oldUser = state.users.find(usersItem => usersItem.id === users.id);
		oldUser = user;
	},
	deleteUser (state, user)
	{
		var index = state.users.findIndex(usersItem => usersItem.id === user.id);
		state.users.splice(index, 1);
	},

	setActiveUser (state, user)
	{
		state.activeUser = user;
	}
};

export default mutations
