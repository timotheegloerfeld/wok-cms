
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import store from './store/store.js'
import routes from './routes.js'

Vue.use(Vuex);
Vue.use(Router)

const admin = new Vue({
	el: '#admin',
	store: new Vuex.Store(store),
	router: new Router({routes}),
});
