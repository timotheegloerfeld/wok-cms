
var Macy = require("macy");
var modal = document.getElementById("modal");

document.addEventListener("DOMContentLoaded", function() {
	Macy({ 
		container: "#macy",
		margin: {
			x: 8,
			y: 5,
		}
	});

	var container = document.getElementById("macy");
	for(var i = 0; i < container.children.length; i++) {
		container.children[i].addEventListener("click", openModal);
	}
	modal.children[modal.children.length-1].addEventListener("click", closeModal);
	modal.children[1].addEventListener("click", openPrev);
	modal.children[3].addEventListener("click", openNext);
});

function openModal(event, direction) {
	if(direction > 0) {
		modal.content = modal.content.nextElementSibling;
	} else if (direction < 0) {
		modal.content = modal.content.previousElementSibling;
	} else {
		modal.content = this;
	}
	
	modal.children[2].appendChild(modal.content.children[0].cloneNode());

	modal.children[1].classList.remove("is-invisible");
	modal.children[3].classList.remove("is-invisible");
	if(!modal.content.previousElementSibling) {
		modal.children[1].classList.add("is-invisible");
	} else if (!modal.content.nextElementSibling) {
		modal.children[3].classList.add("is-invisible");	
	}
	modal.classList.add("is-active");
}

function openNext() {
	modal.children[2].children[0].remove();
	openModal(null, 1);
}

function openPrev() {
	modal.children[2].children[0].remove();
	openModal(null, -1);
}

function closeModal(event) {
	modal.children[2].children[0].remove();
	modal.classList.remove("is-active");
}
