var meals = {};
var mealSection = document.querySelectorAll("#meals")[0];
var categoryColumns = document.querySelectorAll("#categories")[0];

document.addEventListener("DOMContentLoaded", function() {
	mealSection.remove();
	for (var i = 0; i < mealSection.children[0].children.length; i++) {
		var list = mealSection.children[0].children[i];
		var id = list.id.split("-")[1];
		meals[id] = [i, list];
	}
	clear(mealSection.children[0].children);
    var categoryLinks = document.querySelectorAll(".category");
	categoryLinks.forEach(function(category) {
        category.addEventListener("click", showMeals)
	});
});

function showMeals(event) {
	clearClasses(categoryColumns.children);
	clear(mealSection.children[0].children);
	mealSection.remove();
	// will delete all other classes!
	this.children[1].classList.add("is-active");
	var categoryElement = this;
	var categoryId = categoryElement.dataset.id;
	var meal = meals[categoryId];
	mealSection.children[0].appendChild(meal[1]);
	var last = getLastInRow(categoryColumns, meal[0]);
	categoryColumns.insertBefore(mealSection, last.nextSibling);
}

function getLastInRow(columns, elementIndex) {
	var last = false;
	for (var i = elementIndex; i < columns.children.length; i++) {
		//console.log(i);
		if(last && last.offsetTop != columns.children[i].offsetTop) {
			return last;
		}
		last = columns.children[i];
	}
	return last;
}

function clear(list) {
	while(list[0]) {
		list[0].remove();
	}
}

function clearClasses(list) {
	for(var i = 0; i < list.length; i++) {
		//checks that element is not MealsSection
		if(list[i].className.indexOf("fifth") > 0) {
			//removes all classes!
			if(list[i].children[0].children[1].classList.contains("is-active")) {
				list[i].children[0].children[1].classList.remove("is-active");	
			}
		}
	}
}
