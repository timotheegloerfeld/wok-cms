
@extends('front.base')

@section('title', 'Gallerie')

@section('tabs')

<div class="tabs is-centered">
	<ul>
		<li><a href="/">Menu</a></li>
		<li class="is-active"><a href="/gallerie">Gallerie</a></li>
	</ul>
</div>

@endsection

@section('content')

<div id="macy">
	@foreach($pictures as $picture)

	<div><img src="{{ $picture->path }}" alt="{{ $picture->caption }}"></div>

	@endforeach
</div>

<div class="modal" id="modal">
	<div class="modal-background"></div>
	<div class="prev">
		<i class="mdi mdi-chevron-left mdi-48px"></i>
	</div>

	<div class="modal-content is-clipped"></div>

	<span class="next">
		<i class="mdi mdi-chevron-right mdi-48px"></i>
	</span>
	<button class="modal-close is-large" aria-label="close"></button>
</div>


@endsection

@section('scripts')

<script src="{{ asset('js/gallery.js') }}"></script>

@endsection
