@extends('front.base')

@section('title', 'Willkommen')

@section('tabs')

<div class="tabs is-centered">
	<ul>
		<li class="is-active"><a href="/">Menu</a></li>
		<li><a href="/gallerie">Gallerie</a></li>
	</ul>
</div>

@endsection

@section('content')

<div class="columns is-multiline" id="categories">
@foreach ($categories as $category)
<div class="column is-one-fifth">
	<div class="category" data-id="{{ $category->id }}">
		<div class="category-image">
			<figure class="image is-3by2 is-centered is-clipped">
				<img src="{{ $category->picture }}" alt="{{ $category->name }}">
			</figure>
		</div>
		<div class="category-name">
			<a>{{ $category->name }}</a>
		</div>
	</div>
</div>
@endforeach
<div class="section meals column is-full" id="meals">
	<table class="table">
	@foreach ($categories as $category)
		<tbody id="category-{{ $category->id }}">
			@foreach($category->meals as $meal)
			<tr>
				<td>{{ $meal->meal_number }}</td>
				<td>
					<span class="has-text-weight-bold">{{ $meal->name }} </span> 
					{{ $meal->description }}
				</td>
				<td>{{ $meal->price }} €</td>
			</tr>
			@endforeach
		</tbody>
	@endforeach
	</table>
</div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('js/menu.js') }}"></script>

@endsection
