
@extends('front.base')

@section('title', 'Impressum')

@section('content')

<h1 class="title">Impressum</h1>

<p>
Inhaber: Max Mustermann <br>
Restaurant Wok <br>
O7 22 <br>
68161 Mannheim <br>
E-MAil: max.mustermann@wok.de <br>
Tel.: +49 160 1111 1111 <br>
</p>

<h2 class="subtitle is-4 notice">Rechtliche Hinweise zu Webseite</h2>

<p>
Alle Texte, Bilder und weiter hier veröffentlichten Informationen unterliegen dem Urheberrecht des Anbieters, soweit nicht Urheberrechte Dritter bestehen. In jedem Fall ist eine Vervielfältigung, Verbreitung oder öffentliche Wiedergabe ausschließlich im Falle einer widerruflichen und nicht übertragbaren Zustimmung des Anbieters gestattet.
</p>

@endsection
