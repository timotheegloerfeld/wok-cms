@extends('front.base')

@section('title', 'Über uns')

@section('content')

<div class="columns">
	<div class="column is-7">
		<h1 class="title">Über uns</h1>

		<p>
		Lorem ipsum dolor sit amet, est id fastidii platonem, an his omnium integre theophrastus. Te usu mollis dolorem, est volumus scripserit id. Id eos efficiendi referrentur intellegebat. Usu ex probo graeco, ea persius ponderum iracundia qui. Per ad altera facilisis, vis ut delectus officiis expetenda.
		</p>

		<p>
		Eos quas oporteat patrioque eu, ad stet eros justo vim. No exerci aperiam vim, ne his mazim detraxit maluisset. Detracto indoctum theophrastus eos eu, ne graeci iudicabit quo. Et dicat verear impedit vel, mea id adhuc dicit constituto. Est fastidii elaboraret cu, ex duo doming feugiat aliquando.
		</p>
	</div>
	<div class="column is-offset-1">
		<img src="{{ asset('images/restaurant_outdoors.jpg') }}" alt="Asia Wok Außenbereich">
	</div>
</div>



@endsection
