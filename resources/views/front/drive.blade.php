
@extends('front.base')

@section('title', 'Anfahrt')

@section('content')

<div class="columns">
	<div class="column is-6">
		<h1 class="title">Anfahrt</h1>
		
		<p>
			Lorem ipsum dolor sit amet, est id fastidii platonem, an his omnium integre theophrastus. Te usu mollis dolorem, est volumus scripserit id. Id eos efficiendi referrentur intellegebat. Usu ex probo graeco, ea persius ponderum iracundia qui. Per ad altera facilisis, vis ut delectus officiis expetenda.
		</p>
	</div>
	<div class="column">
		<div class="section map">
			<div id="map"></div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
function initMap() {
    var asiaWok = {lat: 49.223656, lng: 8.521404};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: asiaWok
	});

	var service = new google.maps.places.PlacesService(map);
	service.getDetails({
    	placeId: 'ChIJh0MzA7q6l0cRdeeWuaWZfvI'
	}, function (result, status) {
    	var marker = new google.maps.Marker({
        	map: map,
        	place: {
            	placeId: 'ChIJh0MzA7q6l0cRdeeWuaWZfvI',
            	location: result.geometry.location
        	}
		});
	});
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiu30d412-GkXa3rjYP6BZidjmSYqybs8&v=3&libraries=places&callback=initMap"></script>
@endsection
