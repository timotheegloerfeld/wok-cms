<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wok - @yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/front.css') }}">
    <link rel="shortcut icon" sizes="32x32" href="{{ asset('favicon.ico') }}">
</head>
<body>
	<section class="section menu">
		<div class="container">
			<div class="columns">
				<div class="column is-2 is-offset-5">
					<figure class="image is-4by3">
						<img src="/logo.png" alt="logo">
					</figure>
				</div>
			</div>
			
			@section('tabs')
			<div class="tabs is-centered">
				<ul>
					<li><a href="/">Menu</a></li>
					<li><a href="/gallerie">Gallerie</a></li>
				</ul>
			</div>
			@stop

			@yield('tabs')

		</div>
	</section>
	<section class="section">
		<div class="container">
			@yield('content')
		</div>
	</section>
	<footer class="footer">
		<div class="container">
			<div class="columns">
				<div class="column is-3 is-offset-1">
					<ul>
						<li><a href="/ueber-uns">Über uns</a></li>
						<li><a href="/impressum">Impressum</a></li>
						<li><a href="/anfahrt">Anfahrt</a></li>
					</ul>
				</div>
				<div class="column is-3 is-offset-5">
					<ul>
						<li>Wok CMS</li>
						<li>O7 22</li>
						<li>68161 Mannheim</li>
					</ul>
				</div>
			</div>
			<div class="copyright"> © by Wok CMS 2019 </div>
		</div>
	</footer>
	@yield('scripts')
</body>
</html>
