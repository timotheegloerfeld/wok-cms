<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Wok Admin</title>

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="shortcut icon" sizes="32x32" href="{{ asset('favicon.ico') }}">
</head>
<body>
	<div id="admin">
		<router-view></router-view>
	</div>

    <script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
