<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('logout', function() {})->name('logout');

Route::post('login', 'LoginController@login');

Route::get('categories', 'CategoryController@retrieve');
Route::post('categories', 'CategoryController@create');
Route::put('categories/{category}', 'CategoryController@update');
Route::delete('categories/{category}', 'CategoryController@delete');

Route::get('meals', 'MealController@retrieve');
Route::post('meals', 'MealController@create');
Route::put('meals/{meal}', 'MealController@update');
Route::delete('meals/{meal}', 'MealController@delete');

Route::get('pictures', 'PictureController@retrieve');
Route::post('pictures', 'PictureController@create');
Route::put('pictures/{picture}', 'PictureController@update');
Route::delete('pictures/{picture}', 'PictureController@delete');

Route::get('users', 'UserController@retrieve');
Route::post('users', 'UserController@create');
Route::put('users/{user}', 'UserController@update');
Route::delete('users/{user}', 'UserController@delete');
