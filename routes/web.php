<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Picture;

Route::get('admin', function () {
    return view('admin');
});

Route::get('/', function () {
	return view('front.index', ['categories' => Category::all()]);
});

Route::get('gallerie', function () {
	return view('front.gallery', ['pictures' => Picture::all()]);
});

Route::get('ueber-uns', function () {
	return view('front.aboutUs');
});

Route::get('impressum', function () {
	return view('front.siteNotice');
});

Route::get('anfahrt', function () {
	return view('front.drive');
});
