/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 109);
/******/ })
/************************************************************************/
/******/ ({

/***/ 109:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(110);


/***/ }),

/***/ 110:
/***/ (function(module, exports) {

var meals = {};
var mealSection = document.querySelectorAll("#meals")[0];
var categoryColumns = document.querySelectorAll("#categories")[0];

document.addEventListener("DOMContentLoaded", function () {
	mealSection.remove();
	for (var i = 0; i < mealSection.children[0].children.length; i++) {
		var list = mealSection.children[0].children[i];
		var id = list.id.split("-")[1];
		meals[id] = [i, list];
	}
	clear(mealSection.children[0].children);
	var mealLinks = document.querySelectorAll(".category");
	mealLinks.forEach(function (meal) {
		meal.addEventListener("click", showMeals);
	});
});

function showMeals(event) {
	clearClasses(categoryColumns.children);
	clear(mealSection.children[0].children);
	mealSection.remove();
	// will delete all other classes!
	this.children[1].classList.add("is-active");
	var categoryElement = this;
	var categoryId = categoryElement.dataset.id;
	var meal = meals[categoryId];
	mealSection.children[0].appendChild(meal[1]);
	var last = getLastInRow(categoryColumns, meal[0]);
	categoryColumns.insertBefore(mealSection, last.nextSibling);
}

function getLastInRow(columns, elementIndex) {
	var last = false;
	for (var i = elementIndex; i < columns.children.length; i++) {
		//console.log(i);
		if (last && last.offsetTop != columns.children[i].offsetTop) {
			return last;
		}
		last = columns.children[i];
	}
	return last;
}

function clear(list) {
	while (list[0]) {
		list[0].remove();
	}
}

function clearClasses(list) {
	for (var i = 0; i < list.length; i++) {
		//checks that element is not MealsSection
		if (list[i].className.indexOf("fifth") > 0) {
			//removes all classes!
			if (list[i].children[0].children[1].classList.contains("is-active")) {
				list[i].children[0].children[1].classList.remove("is-active");
			}
		}
	}
}

/***/ })

/******/ });