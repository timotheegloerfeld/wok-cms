<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Picture;

class PictureController extends Controller
{
	public function __construct()
	{
    	$this->middleware('auth:api');
	}
	
	public function retrieve()
	{
		return Picture::all();
	}
	
	public function create(Request $request)
	{
		$validatedData = $request->validate([
			'picture' => 'required|image',
			'caption' => 'required|string|max:255',
		]);

		$path = $validatedData['picture']->store('images');

		$picture = new Picture;
		$picture->path = $path;
		$picture->caption = $validatedData['caption'];
		$picture->save();
		return $picture;
	}

	//TODO delete old picture
	public function update(Request $request, Picture $picture)
	{
		$validatedData = $request->validate([
			'caption' => 'required|string|max:255',
		]);

		$picture->caption = $validatedData['caption'];
		$picture->save();
		return $picture;
	}

	public function delete(Picture $picture)
	{
		Storage::delete($picture->path);
		$picture->delete();
	}
}
