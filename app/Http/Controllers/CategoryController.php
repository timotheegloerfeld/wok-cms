<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;

class CategoryController extends Controller
{
	public function __construct()
	{
    	$this->middleware('auth:api');
	}

	public function retrieve()
	{
		return Category::all();
	}

	public function create(Request $request)
	{
		$validatedData = $request->validate([
			'name' => 'required|string|max:255',
			'picture' => 'image',
		]);
		$category = new Category;
		$category->name = $validatedData['name'];
		$category->picture = $validatedData['picture']->store('images');
		$category->save();
		return $category;	
	}

	public function update(Request $request, Category $category)
	{
		$validatedData = $request->validate([
			'name' => 'required|string|max:255',
			'picture' => 'image|nullable',
		]);
		$category->name = $validatedData['name'];
		if(isset($category->picture)) {
			$category->picture = $validatedData['picture']->store('images');
		}
		$category->save();
		return $category;	
	}

	public function delete(Category $category)
	{
		Storage::delete($category->picture);
		$category->delete();
	}
}
