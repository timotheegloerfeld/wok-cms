<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
	public function __construct()
	{
    	$this->middleware('auth:api');
	}
	
	public function retrieve()
	{
		return User::all();
	}
	
	public function create(Request $request)
	{
		$validatedData = $request->validate([
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);

		$user = new User;
		$user->name = $validatedData['name'];
		$user->email = $validatedData['email'];
		$user->password = Hash::make($validatedData['password']);
		$user->save();

		return $user;	
	}
	
	public function update(Request $request, User $user)
	{
		$validatedData = $request->validate([
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255',
			'password' => 'string|min:6|confirmed',
		]);

		$user->name = $validatedData['name'];
		$user->email = $validatedData['email'];
		if(isset($validatedData['password']))
		{
			$user->password = Hash::make($validatedData['password']);
		}

		$user->save();

		return $user;	
	}
	
	public function delete(User $user)
	{
		$user->delete();
	}
}
