<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;

class MealController extends Controller
{
	public function __construct()
	{
    	$this->middleware('auth:api');
	}
	
	public function retrieve()
	{
		return Meal::all();
	}

	public function create(Request $request)
	{
		$validatedData = $request->validate([
			'category_id' => 'required|numeric',
			'meal_number' => 'required|alpha_num|max:255',
			'name' => 'required|string|max:255',
			'description' => 'required|string|max:255',
			'price' => 'required|numeric'
		]);
		$meal = new Meal;
		$meal->category_id = $validatedData["category_id"];
		$meal->meal_number = $validatedData["meal_number"];
		$meal->name = $validatedData["name"];
		$meal->description = $validatedData["description"];
		$meal->price = $validatedData["price"];
		$meal->save();
		return $meal;
	}

	public function update(Request $request, Meal $meal)
	{
		$validatedData = $request->validate([
			'meal_number' => 'required|alpha_num|max:255',
			'name' => 'required|string|max:255',
			'description' => 'required|string|max:255',
			'price' => 'required|numeric'
		]);
		$meal->meal_number = $validatedData["meal_number"];
		$meal->name = $validatedData["name"];
		$meal->description = $validatedData["description"];
		$meal->price = $validatedData["price"];
		$meal->save();
		return $meal;
	}

	public function delete(Meal $meal)
	{
		$meal->delete();
	}
}
